﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using System;

namespace BMGLabtech.Model
{
  /// <summary>
  /// Ein Beispielhaftes Model
  /// </summary>
  public class ExampleModel
  {
    /// <summary>
    /// Die Temperatur im Gerät
    /// </summary>
    private double _temperature;
    public double Temperature
    {
      get { return _temperature; }
      set
      {
        if (Math.Abs(_temperature - value) > 0.1)
        {
          _temperature = value;
          OnTemperatureChanged(this, EventArgs.Empty);
        }
      }
    }

    /// <summary>
    /// Ein Zufallszahlengenerator
    /// </summary>
    private Random random = new Random();

    /// <summary>
    /// Das Event wird gefeuert wenn sich die Temperatur ändert
    /// </summary>
    public event EventHandler TemperatureChanged;

    public void MeasureTemperature()
    {
      Temperature = random.Next(20, 30);

    }

    /// <summary>
    /// Wird aufgerufen wenn sich die Temperatur ändert. Feuert das Event
    /// TemperatureChanged.
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="e"></param>
    private void OnTemperatureChanged(Object obj, EventArgs e)
    {
      TemperatureChanged?.Invoke(obj, e);
    }
  }
}
