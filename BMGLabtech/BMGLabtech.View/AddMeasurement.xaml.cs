﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using System.Windows.Input;

namespace BMGLabtech.View
{
  /// <summary>
  /// Interaktionslogik für AddMeasurement.xaml
  /// </summary>
  public partial class AddMeasurement
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public AddMeasurement()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Ermöglicht das verschieben des Fensters
    /// </summary>
    /// <param name="sender">Der Sender des Events</param>
    /// <param name="e">Die MouseButtonEvent Argumente</param>
    private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      this.DragMove();
    }
  }
}
