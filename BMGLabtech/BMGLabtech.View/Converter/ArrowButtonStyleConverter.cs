﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using System;
using System.Windows;
using System.Windows.Data;

namespace BMGLabtech.View.Converter
{
  class ArrowButtonStyleConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      bool isChecked = (bool)values[0];
      Style arrowButton = values[1] as Style;
      Style rectangularButton = values[2] as Style;

      return isChecked ? arrowButton : rectangularButton;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotSupportedException();
    }
  }
}
