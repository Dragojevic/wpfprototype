﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace BMGLabtech.View.Converter
{
  class FirstValueMinusSecondValue : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double rootHeight = (double)values[0];
      
      double headerHeight = (double)values[1];
      double greyOutRectangleHeight = rootHeight - headerHeight;

      return greyOutRectangleHeight;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotSupportedException();
    }
  }
}