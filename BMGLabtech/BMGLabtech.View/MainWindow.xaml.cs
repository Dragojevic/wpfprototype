﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************

using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace BMGLabtech.View
{
  /// <summary>
  /// Interaktionslogik für MainWindow.xaml
  /// </summary>
  public partial class MainWindow : INotifyPropertyChanged
  {
    private bool _isGrayOut;
    public bool IsGrayOut
    {
      get { return _isGrayOut;}
      set
      {
        _isGrayOut = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Das Event das gefeuert wird wenn sich eine Poperty geändert hat
    /// </summary>
    public event PropertyChangedEventHandler PropertyChanged;

   /// <summary>
    /// Konstruktor
    /// </summary>
    public MainWindow()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Ermöglicht das Verschieben des Fensters
    /// </summary>
    /// <param name="sender">Der Sender des Events</param>
    /// <param name="e">Die MouseButtonEvent Argumente</param>
    private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      this.DragMove();
    }

    /// <summary>
    /// Wird aufgerufen wenn sich eine Property ändert. Löst das Event PropertyChanged aus.
    /// </summary>
    /// <param name="propertyName">Der Name der Poperty die sich geändert hat. Wird der Name nicht
    /// mit gegeben, dann wird der Name der aufrufenden Funktion verwendet</param>
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

  }
}
