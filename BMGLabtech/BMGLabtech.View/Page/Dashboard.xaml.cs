﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.Page
{
  /// <summary>
  /// Interaktionslogik für Dashboard.xaml
  /// </summary>
  public partial class Dashboard
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public Dashboard()
    {
      InitializeComponent();
    }
  }
}
