﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.Page
{
  /// <summary>
  /// Interaktionslogik für Library.xaml
  /// </summary>
  public partial class Library
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public Library()
    {
      InitializeComponent();
    }
  }
}
