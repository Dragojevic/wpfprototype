﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.Page
{
  /// <summary>
  /// Interaktionslogik für MeasurementMethod.xaml
  /// </summary>
  public partial class MeasurementMethod
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public MeasurementMethod()
    {
      InitializeComponent();
    }
  }
}
