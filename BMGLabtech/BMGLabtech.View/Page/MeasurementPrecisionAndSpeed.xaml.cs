﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************

namespace BMGLabtech.View.Page
{
  /// <summary>
  /// Interaktionslogik für MeasurementPrecisionAndSpeed.xaml
  /// </summary>
  public partial class MeasurementPrecisionAndSpeed
  {
    public MeasurementPrecisionAndSpeed()
    {
      InitializeComponent();
    }
  }
}
