﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.Page
{
  /// <summary>
  /// Interaktionslogik für MeasurementWavelength.xaml
  /// </summary>
  public partial class MeasurementWavelength
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public MeasurementWavelength()
    {
      InitializeComponent();
    }
  }
}
