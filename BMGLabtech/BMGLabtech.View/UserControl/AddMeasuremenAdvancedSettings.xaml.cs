﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für AddMeasuremenAdvancedSettings.xaml
  /// </summary>
  public partial class AddMeasuremenAdvancedSettings
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public AddMeasuremenAdvancedSettings()
    {
      InitializeComponent();
    }
  }
}
