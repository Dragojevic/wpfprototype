﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für AdminSelection.xaml
  /// </summary>
  public partial class AdminSelection
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public AdminSelection()
    {
      InitializeComponent();
    }
  }
}
