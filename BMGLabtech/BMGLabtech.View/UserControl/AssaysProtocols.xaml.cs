﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für AssaysProtocols.xaml
  /// </summary>
  public partial class AssaysProtocols
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public AssaysProtocols()
    {
      InitializeComponent();
    }
   
  }
}
