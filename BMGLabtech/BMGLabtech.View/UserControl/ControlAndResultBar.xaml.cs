﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für ControlAndResultBar.xaml
  /// </summary>
  public partial class ControlAndResultBar
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public ControlAndResultBar()
    {
      InitializeComponent();
    }
  }
}
