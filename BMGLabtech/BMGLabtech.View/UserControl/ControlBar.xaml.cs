﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für ControlBar.xaml
  /// </summary>
  public partial class ControlBar
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public ControlBar()
    {
      InitializeComponent();
    }
  }
}
