﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für DeviceSelection.xaml
  /// </summary>
  public partial class DeviceSelection
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public DeviceSelection()
    {
      InitializeComponent();
    }
  }
}
