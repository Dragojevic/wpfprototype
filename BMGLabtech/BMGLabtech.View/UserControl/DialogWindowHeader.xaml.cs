﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using System.Windows;

namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für CloseWindow.xaml
  /// </summary>
  public partial class DialogWindowHeader
  {
    /// <summary>
    /// Dependency Property für ContentText
    /// </summary>
    public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(DialogWindowHeader));

    /// <summary>
    /// Konstruktor
    /// </summary>
    public DialogWindowHeader()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Hiermit wird der Content des Buttons gesetzt
    /// </summary>
    public string Text
    {
      get { return (string)GetValue(TextProperty); }
      set { SetValue(TextProperty, value); }
    }

    /// <summary>
    /// Wird aufgerufen wenn auf die Schaltfläche zum schließen des Fensters gedrückt wird. Schließt das Fenster.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonCloseClick(object sender, RoutedEventArgs e)
    {
      Window parentWindow = Window.GetWindow(this);
      parentWindow?.Close();
    }
  }
}
