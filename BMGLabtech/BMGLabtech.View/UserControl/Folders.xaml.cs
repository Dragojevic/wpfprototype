﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für Folders.xaml
  /// </summary>
  public partial class Folders
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public Folders()
    {
      InitializeComponent();
    }
  }
}
