﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für Header.xaml
  /// </summary>
  public partial class Header
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public Header()
    {
      InitializeComponent();
    }
    
  }
}

