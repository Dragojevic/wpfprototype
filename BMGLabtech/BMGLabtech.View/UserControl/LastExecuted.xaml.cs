﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für LastExecuted.xaml
  /// </summary>
  public partial class LastExecuted
  {
    /// <summary>
    ///  Konstruktor
    /// </summary>
    public LastExecuted()
    {
      InitializeComponent();
    }
  }
}
