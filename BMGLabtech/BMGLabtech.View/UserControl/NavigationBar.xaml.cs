﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using BMGLabtech.ViewModel;
using System;
using System.Globalization;
using System.Windows;
using WPFLocalizeExtension.Engine;

namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für NavigationBar.xaml
  /// </summary>
  public partial class NavigationBar
  {
    //TODO-DD Folgende Zeile muss wieder raus. Feld nur für Test eingebaut
    private int _number;

    /// <summary>
    /// Konstruktor
    /// </summary>
    public NavigationBar()
    {
      InitializeComponent();
      NavigateToDashboard.IsChecked = true;
    }

    /// <summary>
    /// Deselektiert alle Buttons in der NavigationBar und selektiert den
    /// Button NavigateToDashboard. Sorgt für die Änderung der Darstellung.
    /// </summary>
    /// <param name="sender">Der Sender des Events</param>
    /// <param name="e">Die Event Argumente</param>
    private void NavigateToDashboard_OnClicked(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Deselektiert alle Buttons in der NavigationBar und selektiert den
    /// Button NavigateToLibrary. Sorgt für die Änderung der Darstellung.
    /// </summary>
    /// <param name="sender">Der Sender des Events</param>
    /// <param name="e">Die Event Argumente</param>
    private void NavigateToLibrary_OnClicked(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Deselektiert alle Buttons in der NavigationBar und selektiert den
    /// Button NavigateToMeasures. Sorgt für die Änderung der Darstellung.
    /// </summary>
    /// <param name="sender">Der Sender des Events</param>
    /// <param name="e">Die Event Argumente</param>
    private void NavigateToMeasures_OnClicked(object sender, EventArgs e)
    {
      // TODO-DD: Das folgende ist nur temporär. Das öffnen neuer Seiten im MVVM muss noch implementiert werden
      // TODO-DD: Verweise auf ViewModel und View aus dem Projekt wieder raus nehmen!!!
      AddMeasurement addMeasurement = new AddMeasurement();
      var viewModel = this.DataContext as MainWindowViewModel;
      if (viewModel != null)
      {
        addMeasurement.DataContext = new AddMeasurementViewModel(viewModel.Model);
      }
      MainWindow parentWindow = Window.GetWindow(this) as MainWindow;
      if (parentWindow != null)
      {
        parentWindow.IsGrayOut = true;
        addMeasurement.Owner = parentWindow;
        addMeasurement.ShowDialog();
        parentWindow.IsGrayOut = false;
      }
    }

    /// <summary>
    /// Deselektiert alle Buttons in der NavigationBar und selektiert den
    /// Button NavigateToSettings. Sorgt für die Änderung der Darstellung.
    /// </summary>
    /// <param name="sender">Der Sender des Events</param>
    /// <param name="e">Die Event Argumente</param>
    private void NavigateToSettings_OnClicked(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Deselektiert alle Buttons in der NavigationBar und selektiert den
    /// Button NavigateToService. Sorgt für die Änderung der Darstellung.
    /// </summary>
    /// <param name="sender">Der Sender des Events</param>
    /// <param name="e">Die Event Argumente</param>
    private void NavigateToService_OnClicked(object sender, EventArgs e)
    {
      //TODO-DD Folgende Zeile muss wieder raus
      LocalizeDictionary.Instance.Culture = (_number % 2 == 0) ? new CultureInfo("de-DE") : new CultureInfo("en-GB");
      _number++;
    }
  }
}
