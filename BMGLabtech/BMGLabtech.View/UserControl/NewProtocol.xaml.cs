﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für NewProtocol.xaml
  /// </summary>
  public partial class NewProtocol
  {
    /// <summary>
    ///  Konstruktor
    /// </summary>
    public NewProtocol()
    {
      InitializeComponent();
    }
  }
}
