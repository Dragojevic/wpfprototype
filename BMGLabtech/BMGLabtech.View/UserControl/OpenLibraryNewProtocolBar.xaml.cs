﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für OpenLibraryNewProtocolBar.xaml
  /// </summary>
  public partial class OpenLibraryNewProtocolBar
  {
    public OpenLibraryNewProtocolBar()
    {
      InitializeComponent();
    }
  }
}
