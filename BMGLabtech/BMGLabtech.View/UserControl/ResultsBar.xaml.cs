﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für ResultsBar.xaml
  /// </summary>
  public partial class ResultsBar
  {
    public ResultsBar()
    {
      InitializeComponent();
    }
  }
}
