﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für StatusBar.xaml
  /// </summary>
  public partial class StatusBar
  {
    /// <summary>
    /// Konstruktor
    /// </summary>
    public StatusBar()
    {
      InitializeComponent();
    }
  }
}
