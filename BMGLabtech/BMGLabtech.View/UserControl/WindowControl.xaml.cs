﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using System.Windows;

namespace BMGLabtech.View.UserControl
{
  /// <summary>
  /// Interaktionslogik für WindowControl.xaml
  /// </summary>
  public partial class WindowControl
  {
    public WindowControl()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Wird aufgerufen wenn auf die Schaltfläche zum schließen des Fensters gedrückt wird. Schließt das Fenster.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonCloseClick(object sender, RoutedEventArgs e)
    {
      Window parentWindow = Window.GetWindow(this);
      parentWindow?.Close();
    }

    /// <summary>
    /// Wird aufgerufen wenn auf die Schaltfläche zum Minimieren gedrückt wurde. Minimiert das Fenster.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonMinimizeClick(object sender, RoutedEventArgs e)
    {
      Window parentWindow = Window.GetWindow(this);
      if (parentWindow != null)
      {
        parentWindow.WindowState = WindowState.Minimized;
      }
    }

    /// <summary>
    /// Setzt den Status des Fensters entweder auf Normal falls das Fenster Maximized dargestellt ist. Sonst wird
    /// der Status auf Maximized gesetzt.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ButtonMaximizeOrNormalClick(object sender, RoutedEventArgs e)
    {
      Window parentWindow = Window.GetWindow(this);
      if (parentWindow != null)
      {
        parentWindow.WindowState = (parentWindow.WindowState == WindowState.Maximized) ? WindowState.Normal : WindowState.Maximized;
      }
    }

  }
}
