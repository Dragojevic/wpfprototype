﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using BMGLabtech.Model;
using System.Windows.Input;
using BMGLabtech.ViewModel.Page;

namespace BMGLabtech.ViewModel
{
  /// <summary>
  /// Der DataContext für die View AddMeasurement
  /// </summary>
  public class AddMeasurementViewModel : ViewModelBase
  {
    /// <summary>
    /// Das Model
    /// </summary>
    private ExampleModel _model;


    private ViewModelBase _selectedViewModel;
    /// <summary>
    /// Das ViewModel der View die gerade angezeigt werden soll
    /// </summary>
    public ViewModelBase SelectedViewModel
    {
      get => _selectedViewModel;
      set
      {
        _selectedViewModel = value;
        OnPropertyChanged();
      }
    }

    private readonly RelayCommand _navigateToMethodSettings;
    /// <summary>
    /// Das Kommando über dem zu Dashboard navigiert wird
    /// </summary>
    public ICommand NavigateToMethodSettings
    {
      get => _navigateToMethodSettings;
    }

    private readonly RelayCommand _navigateToWavelengthSettings;
    /// <summary>
    /// Das Kommando über dem zu Dashboard navigiert wird
    /// </summary>
    public ICommand NavigateToWavelengthSettings
    {
      get => _navigateToWavelengthSettings;
    }

    private readonly RelayCommand _navigateToPrecisionAndSpeedSettings;
    /// <summary>
    /// Das Kommando über dem zu Dashboard navigiert wird
    /// </summary>
    public ICommand NavigateToPrecisionAndSpeedSettings
    {
      get => _navigateToPrecisionAndSpeedSettings;
    }

    /// <summary>
    /// Konstruktor
    /// </summary>
    /// <param name="model">Das Model</param>
    public AddMeasurementViewModel(ExampleModel model)
    {
      _model = model;
      SelectedViewModel = new MeasurementMethodViewModel();
      _navigateToMethodSettings = new RelayCommand(parameter => DoNavigateToMethodSettings());
      _navigateToWavelengthSettings = new RelayCommand(parameter => DoNavigateToWavelengthSettings());
      _navigateToPrecisionAndSpeedSettings = new RelayCommand(parameter => DoNavigateToPrecisionAndSpeedSettings());
    }

    /// <summary>
    /// Navigiert zur Measurement Methods Seite
    /// </summary>
    private void DoNavigateToMethodSettings()
    {
      SelectedViewModel = new MeasurementMethodViewModel();
    }

    /// <summary>
    /// Navigiert zur Measurement Wavelength Seite
    /// </summary>
    private void DoNavigateToWavelengthSettings()
    {
      SelectedViewModel = new MeasurementWavelengthViewModel(_model);
    }

    /// <summary>
    /// Navigiert zur Measurement Precision And Speed Seite
    /// </summary>
    private void DoNavigateToPrecisionAndSpeedSettings()
    {
      SelectedViewModel = new MeasurementPrecisionAndSpeedViewModel(_model);
    }

  }
}
