﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using System;
using System.Windows.Input;
using BMGLabtech.Model;
using BMGLabtech.ViewModel.Page;

namespace BMGLabtech.ViewModel
{
  /// <summary>
  /// Der DataContext für MainWindow
  /// </summary>
  public class MainWindowViewModel : ViewModelBase
  {
    /// <summary>
    /// Das Model
    /// </summary>
    private ExampleModel _model;

    // TODO-DD wieder löschen das ist zu Testzwecken drin
    public ExampleModel Model { get; set; }

    private ViewModelBase _selectViewModel;
    /// <summary>
    /// Das ViewModel der View die gerade angezeigt werden soll
    /// </summary>
    public ViewModelBase SelectedViewModel
    {
      get => _selectViewModel;
      set
      {
        _selectViewModel = value;
        OnPropertyChanged();
      }
    }

    private readonly RelayCommand _navigateToDashboard;
    /// <summary>
    /// Das Kommando über dem zu Dashboard navigiert wird
    /// </summary>
    public ICommand NavigateToDashboard
    {
      get => _navigateToDashboard;
    }

    private readonly RelayCommand _navigateToLibrary;
    /// <summary>
    /// Das Kommando über dem zu Library navigiert wird
    /// </summary>
    public ICommand NavigateToLibrary
    {
      get => _navigateToLibrary;
    }

    private readonly RelayCommand _navigateToMeasures;
    /// <summary>
    /// Das Kommando über dem zu Measures navigiert wird
    /// </summary>
    public ICommand NavigateToMeasures
    {
      get => _navigateToMeasures;
    }

    private readonly RelayCommand _navigateToService;
    /// <summary>
    /// Das Kommando über dem zu Service navigiert wird
    /// </summary>
    public ICommand NavigateToService
    {
      get => _navigateToService;
    }

    private readonly RelayCommand _navigateToSettings;
    /// <summary>
    /// Das Kommando über dem zu Settings navigiert wird
    /// </summary>
    public ICommand NavigateToSettings
    {
      get => _navigateToSettings;
    }

    /// <summary>
    /// Konstruktor, setzt die Kommandos und registriert einen Event
    /// </summary>
    /// <param name="model"></param>
    public MainWindowViewModel(ExampleModel model)
    {
      _model = model;
      _model.TemperatureChanged += OnTemperatureChanged;
      SelectedViewModel = new DashboardViewModel(model);
      _navigateToDashboard = new RelayCommand(parameter => DoNavigateToDashboard());
      _navigateToLibrary = new RelayCommand(param => DoNavigateToLibrary());
      _navigateToMeasures = new RelayCommand(param => DoNavigateToMeasurement()); // TODO-DD: Zur richtigen Seite navigieren
      _navigateToService = new RelayCommand(param => DoNavigateToLibrary()); // TODO-DD: Zur richtigen Seite navigieren
      _navigateToSettings = new RelayCommand(param => DoNavigateToLibrary()); // TODO-DD: Zur richtigen Seite navigieren
    }

    /// <summary>
    /// Navigiert zur Seite Dashboard indem das selektierte ViewModel auf das DashboardViewModel gesetzt wird
    /// </summary>
    private void DoNavigateToDashboard()
    {
      SelectedViewModel = new DashboardViewModel(_model);
    }

    /// <summary>
    /// Navigiert zur Seite Library indem das selektierte ViewModel auf das DashboardViewModel gesetzt wird
    /// </summary>
    private void DoNavigateToLibrary()
    {
      SelectedViewModel = new LibraryViewModel();
    }

    /// <summary>
    /// Navigiert zur Seite Library indem das selektierte ViewModel auf das DashboardViewModel gesetzt wird
    /// TODO-DD Das ist nur zum Testen hier drin
    /// </summary>
    private void DoNavigateToMeasurement()
    {

    }

    /// <summary>
    /// Die in der GUI gebundene Temperatur
    /// </summary>
    public double Temperature
    {
      get { return _model.Temperature; }
    }

    /// <summary>
    /// Event Handler der aufgerufen wird wenn sich im Model die Temperatur ändert
    /// Aktualisiert den Wert in der GUI
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="e"></param>
    private void OnTemperatureChanged(Object obj, EventArgs e)
    {
      OnPropertyChanged($"Temperature");
    }

  }
}
