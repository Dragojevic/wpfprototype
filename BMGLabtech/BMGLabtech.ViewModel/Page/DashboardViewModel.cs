﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using System.Windows.Input;
using BMGLabtech.Model;

namespace BMGLabtech.ViewModel.Page
{
  /// <summary>
  /// Der DataContext für Dashboard
  /// </summary>
  public class DashboardViewModel : ViewModelBase
  {
    /// <summary>
    /// Das Model
    /// </summary>
    private ExampleModel _model;

    private readonly RelayCommand _updateTemperature;
    /// <summary>
    /// Das Komando über dem die Temperatur aktualisiert wird
    /// </summary>
    public ICommand UpdateTemperature
    {
      get => _updateTemperature;
    }

    /// <summary>
    /// Konstruktor
    /// </summary>
    /// <param name="model"></param>
    public DashboardViewModel(ExampleModel model)
    {
      _model = model;
      _updateTemperature = new RelayCommand(param => DoUpdateTemperature());
    }

    /// <summary>
    /// Aktualisiert die Temperatur in dem eine neue Messung vorgenommen wird
    /// </summary>
    public void DoUpdateTemperature()
    {
      _model.MeasureTemperature();
    }
  }
}
