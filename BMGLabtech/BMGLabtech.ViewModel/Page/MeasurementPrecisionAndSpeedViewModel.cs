﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using BMGLabtech.Model;

namespace BMGLabtech.ViewModel.Page
{
  public class MeasurementPrecisionAndSpeedViewModel : ViewModelBase
  {
    /// <summary>
    /// Das Model
    /// </summary>
    // ReSharper disable once NotAccessedField.Local Das ist nur vorübergehend
    private ExampleModel _model;

    /// <summary>
    /// Das Feld für die Property Einstellugsdauer 
    /// </summary>
    private int _settingTime = 3;

    /// <summary>
    /// Die Einstellugsdauer Property
    /// </summary>
    public int SettingTime 
    {
      get => _settingTime;
      set
      {
        _settingTime = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Das Feld für die Property NumberOfFlashesPerWell
    /// </summary>
    private int _numberOfFlashesPerWell = 300;

    /// <summary>
    /// Die Anzahl der Blitze pro Vertiefung
    /// </summary>
    public int NumberOfFlashesPerWell
    {
      get => _numberOfFlashesPerWell;
      set
      {
        _numberOfFlashesPerWell = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Das Feld für die Property PrecisionAndSpeedValue
    /// </summary>
    private double _precisionAndSpeedValue;

    /// <summary>
    /// Der Wert der die Präferenz zwischen Genauigkeit und Geschwindigkeit angibt.
    /// </summary>
    public double PrecisionAndSpeedValue
    {
      get { return _precisionAndSpeedValue; }
      set
      {
        _precisionAndSpeedValue = value;
        SettingTime += (int)(10 * _precisionAndSpeedValue);
        NumberOfFlashesPerWell += (int)(500 * _precisionAndSpeedValue);
      }
    }

    /// <summary>
    /// Konstruktor
    /// </summary>
    /// <param name="model">Das Model</param>
    public MeasurementPrecisionAndSpeedViewModel(ExampleModel model)
    {
      _model = model;
    }
  }
}
