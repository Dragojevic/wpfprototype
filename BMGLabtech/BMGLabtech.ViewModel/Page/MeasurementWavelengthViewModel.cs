﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using BMGLabtech.Model;

namespace BMGLabtech.ViewModel.Page
{
  /// <summary>
  /// Der DataContext für MeasurementWavelength
  /// </summary>
  public class MeasurementWavelengthViewModel : ViewModelBase
  {
    /// <summary>
    /// Das Model
    /// </summary>
    // ReSharper disable once NotAccessedField.Local Das ist nur vorübergehend
    private ExampleModel _model;

    /// <summary>
    /// Konstruktor
    /// </summary>
    /// <param name="model">Das Model</param>
    public MeasurementWavelengthViewModel(ExampleModel model)
    {
      _model = model;
    }
  }
}
