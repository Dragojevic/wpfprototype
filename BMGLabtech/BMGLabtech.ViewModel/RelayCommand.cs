﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using System;
using System.Windows.Input;

namespace BMGLabtech.ViewModel
{
  /// <summary>
  /// Hilfsklasse um Commands einfacher benutzen zu können
  /// </summary>
  class RelayCommand : ICommand
  {
    /// <summary>
    /// Die Aktion die ausgeführt werden soll
    /// </summary>
    private readonly Action<object> _execute;

    /// <summary>
    /// Das Predicate Delegat mit dem bestimmt wird ob die Aktion ausgeführt werden kann oder nicht.
    /// </summary>
    private readonly Predicate<object> _canExecute;

    /// <summary>
    /// Konstruktor
    /// </summary>
    /// <param name="execute">Die Aktion die ausgeführt werden soll</param>
    /// <remarks></remarks>
    public RelayCommand(Action<object> execute)
        : this(execute, null)
    {
    }

    /// <summary>
    /// Konstruktor
    /// </summary>
    /// <param name="execute">Die Aktion die ausgeführt werden soll.</param>
    /// <param name="canExecute">Das Predicate mit dem bestimmt wird ob die Aktion ausgeführt werden kann oder nicht.</param>
    /// <remarks></remarks>
    public RelayCommand(Action<object> execute, Predicate<object> canExecute)
    {
      if (execute == null)
      {
        throw new ArgumentNullException("execute");
      }
      this._execute = execute;
      this._canExecute = canExecute;
    }

    /// <summary>
    /// Ruft das Predicate Delegat auf mit dem bestimmt wird ob die Aktion im aktuellen Zustand ausgeführt werden kann oder nicht.
    /// </summary>
    /// <param name="parameter">Eingabeparameter für das Predicate Delegat. Wenn es keinen Eingabeparmeter benötigt, dann kann dieser Parameter null sein.</param>
    /// <returns>true falls die Aktion ausgeführt werden kann; sonst, false.</returns>
    /// <remarks></remarks>
    public bool CanExecute(object parameter)
    {
      if (_canExecute == null)
      {
        return true;
      }
      return _canExecute(parameter);
    }

    /// <summary>
    /// Das Event das auftritt wenn sich etwas ändert was die Bedingung ändert ob die Aktion ausgeführt werden kann oder nicht.
    /// </summary>
    /// <remarks></remarks>
    public event EventHandler CanExecuteChanged
    {
      add { CommandManager.RequerySuggested += value; }
      remove { CommandManager.RequerySuggested -= value; }
    }

    /// <summary>
    /// Führt die Aktion aus.
    /// </summary>
    /// <param name="parameter">Eingabeparameter der Aktion.  Wenn es keinen Eingabeparmeter benötigt, dann kann dieser Parameter null sein.</param>
    /// <remarks></remarks>
    public void Execute(object parameter)
    {
      _execute(parameter);
    }

  }
}
