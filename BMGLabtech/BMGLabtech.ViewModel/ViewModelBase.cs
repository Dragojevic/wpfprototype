﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// *******************************************************
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BMGLabtech.ViewModel
{
  /// <summary>
  /// Basisklasse für alle ViewModels. Implementiert das INotifyPropertyChanged Interface
  /// </summary>
  public class ViewModelBase : INotifyPropertyChanged
  {
    /// <summary>
    /// Das Event das gefeuert wird wenn sich eine Poperty geändert hat
    /// </summary>
    public event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Wird aufgerufen wenn sich eine Property ändert. Löst das Event PropertyChanged aus.
    /// </summary>
    /// <param name="propertyName">Der Name der Poperty die sich geändert hat. Wird der Name nicht
    /// mit gegeben, dann wird der Name der aufrufenden Funktion verwendet</param>
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
