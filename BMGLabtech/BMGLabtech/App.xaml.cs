﻿// *******************************************************
// <copyright company=”BMG Labtech GmbH”>
// © 2019 BMG Labtech GmbH.
// </copyright>
//
// ******************************************************* 

using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using BMGLabtech.Model;
using BMGLabtech.ViewModel;
using BMGLabtech.View;
using WPFLocalizeExtension.Engine;

namespace BMGLabtech
{
  /// <summary>
  /// Interaktionslogik für "App.xaml"
  /// </summary>
  public partial class App
  {
    /// <summary>
    /// Wird beim starten der Application ausgeführt. Setzt das MainWindow und die Sprache
    /// </summary>
    /// <param name="e">Die Startup Event Argumente</param>
    protected override void OnStartup(StartupEventArgs e)
    {
      base.OnStartup(e);
      if ((IsDomainNameValid() || IsUserNameValid()) && IsWithinValidityPeriod())
      {
        // TODO-DD: Sprache einstellbar machen und Einstellung speichern
        LocalizeDictionary.Instance.SetCurrentThreadCulture = true;
        //LocalizeDictionary.Instance.Culture = System.Threading.Thread.CurrentThread.CurrentUICulture; // new CultureInfo("en-GB");
        LocalizeDictionary.Instance.Culture = new CultureInfo("en-GB");
        ExampleModel model = new ExampleModel();
        MainWindowViewModel mainWindowViewModel = new MainWindowViewModel(model);
        MainWindow mainWindow = new MainWindow();
        mainWindow.DataContext = mainWindowViewModel;
        this.MainWindow = mainWindow;
        this.MainWindow.Show();
        model.MeasureTemperature();
      }
      else
      {
        //var domainName = Environment.UserDomainName;
        //string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('\\').Last();
        //string coumputerName = Environment.MachineName;
        //MessageBox.Show("Die Demoversion ist abgelaufen\nComputername: " + coumputerName + "\n Benutzername: "+ userName+"\nDomäne Name: "+ domainName, "Demoversion abgelaufen", MessageBoxButton.OK, MessageBoxImage.Error);
        MessageBox.Show("Die Demoversion ist abgelaufen", "Demoversion abgelaufen", MessageBoxButton.OK, MessageBoxImage.Error);
        Environment.Exit(0);
      }
    }

    /// <summary>
    /// Prüft ob der aktuelle Domain Name gülig ist oder nicht
    /// </summary>
    /// <returns></returns>
    private bool IsDomainNameValid()
    {
      bool isDomainNameValid = false;
      var domainName = Environment.UserDomainName;
      if (domainName.Equals("CADI") || domainName.Equals("BMGLABTECH"))
      {
        isDomainNameValid = true;
      }

      return isDomainNameValid;
    }

    /// <summary>
    /// Prüft ob der in Windows eingelogte Benutzer ein gültiger Benutzer ist.
    /// </summary>
    /// <returns></returns>
    private bool IsUserNameValid()
    {
      bool isUserNameValid = false;
      string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('\\').Last();
      if (userName.Equals("ebi", StringComparison.InvariantCultureIgnoreCase) || userName.Equals("katharina", StringComparison.InvariantCultureIgnoreCase) || userName.Equals("lukas", StringComparison.InvariantCultureIgnoreCase) || userName.Equals("diana", StringComparison.InvariantCultureIgnoreCase))
      {
        isUserNameValid = true;
      }

      return isUserNameValid;
    }

    /// <summary>
    /// Prüft ob die die Ausführung innerhalb der Gülitgkeitsdauer liegt oder nicht
    /// </summary>
    /// <returns>true die Gültigkeitsdauer ist noch nicht abgelaufen, false sonst</returns>
    private bool IsWithinValidityPeriod()
    {
      bool isWithinValidityPeriod = false;
      DateTime expiryDate = new DateTime(2020, 6, 1, 0, 0, 0);
      int result = DateTime.Compare(expiryDate, DateTime.Today);
      if (result > 0) // expiryDate ist später als DateTime.Today.
      {
        isWithinValidityPeriod = true;
      }

      return isWithinValidityPeriod;
    }

  }
}
